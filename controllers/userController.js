const bcrypt = require("bcrypt");
const Validator = require("fastest-validator");
const v = new Validator();
const { User, Image } = require("../models");

module.exports = {
  createUser: async (req, res) => {
    try {
      const schema = {
        f_name: "string|required",
        l_name: "string|required",
        email: "email|required",
        password: "string|required|min:8",
      };

      const validator = v.validate(req.body, schema);

      if (validator.length) {
        return res.status(400).json({
          status: false,
          message: "bad request!",
          data: validator,
        });
      }

      const emailCheck = await User.findOne({
        where: { email: req.body.email },
      });

      if (emailCheck) {
        return res.status(400).json({
          status: false,
          message: "email already used!",
          data: null,
        });
      }

      const encryptedPass = await bcrypt.hash(req.body.password, 10);
      const newUser = await User.create({
        ...req.body,
        password: encryptedPass,
        role: "user",
        user_type: "basic",
      });

      res.status(201).json({
        status: true,
        message: "registered successfully!",
        data: newUser,
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  updateAvatar: async (req, res) => {
    try {
      const schema = {
        user_id: "number|required",
        image_id: "number|required",
      };

      const validate = v.validate(req.body, schema);
      if (validate.length) {
        return res.status(400).json({
          status: false,
          message: "bad request!",
          data: validate,
        });
      }

      const { user_id, image_id } = req.body;

      const image = await Image.findOne({ where: { id: image_id } });
      if (!image) {
        return res.status(400).json({
          status: false,
          message: "image is doesn't exist!",
          data: null,
        });
      }

      const user = await User.findOne({ where: { id: user_id } });
      if (!user) {
        return res.status(400).json({
          status: false,
          message: "user is doesn't exist!",
          data: null,
        });
      }

      const updatedUser = await User.update(
        {
          avatar: image_id,
        },
        {
          where: { id: user_id },
        }
      );

      res.status(200).json({
        status: true,
        message: "avatar changed succesfully!",
        data: updatedUser,
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  showUser: async (req, res) => {
    try {
      const user_id = req.params.id;

      const user = await User.findOne({ where: { id: user_id } });
      if (!user) {
        return res.status(400).json({
          status: false,
          message: "user doesn't exist!",
          data: null,
        });
      }

      var avatar;
      const image = await Image.findOne({ where: { id: user.avatar } });
      if (image) {
        avatar = image;
        return res.status(200).json({
          status: true,
          message: "ok",
          data: {
            ...user.dataValues,
            avatar: avatar.url,
          },
        });
      }

      res.status(200).json({
        status: true,
        message: "ok",
        data: {
          ...user.dataValues,
        },
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  getUsers: async (req, res) => {
    try {
      let users = await User.findAll();

      res.status(200).json({
        status: "success",
        message: "successfully retrieve all data user",
        data: users,
      });
    } catch (err) {
      res.status(500).json({
        status: "error",
        error: err,
      });
    }
  },
  
  updateUser: async (req, res) => {
    try {
      let { username, password, biodata_id } = req.body;
      let { id } = req.params;
      let query = {
        where: {
          id,
        },
      };
      let updatedUser = await User.update(
        {
          username,
          password,
          biodata_id,
        },
        query
      );

      res.status(200).json({
        status: "success",
        message: "successfully updated data",
        data: updatedUser,
      });
    } catch (err) {
      res.status(500).json({
        status: "error",
        error: err.message,
      });
    }
  },

  deleteUser: async (req, res) => {
    try {
      const user_id = req.params.id;
      let deleted = await User.destroy({
        where: {
          id: user_id,
        },
      });

      res.status(200).json({
        status: "success",
        message: "successfully deleted user with id",
        data: deleted,
      });
    } catch (err) {
      res.status(500).json({
        status: "error",
        error: err.message,
      });
    }
  },
};
