require("dotenv").config();
const { User } = require("../models");
const jwt = require("jsonwebtoken");
const { google } = require("googleapis");
const Validator = require("fastest-validator");
const v = new Validator();

const {
  SERVER_ROOT_URI,
  SERVER_LOGIN_ENDPOINT,
  OAUTH_CLIENT_ID,
  OAUTH_CLIENT_SECRET,
  JWT_SECRET_KEY,
} = process.env;

const oauth2Client = new google.auth.OAuth2(
  OAUTH_CLIENT_ID,
  OAUTH_CLIENT_SECRET,
  `${SERVER_ROOT_URI}/${SERVER_LOGIN_ENDPOINT}`
);

function generateAuthUrl() {
  // define scope mana aja yang bakal diakses sama kredensialnya nanti
  const scopes = [
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/userinfo.profile",
  ];

  const url = oauth2Client.generateAuthUrl({
    access_type: "offline",
    response_type: "code",
    scope: scopes,
  });

  return url;
}

async function setCredentials(code) {
  return new Promise(async (resolve, reject) => {
    try {
      const { tokens } = await oauth2Client.getToken(code);
      oauth2Client.setCredentials(tokens);

      return resolve(tokens);
    } catch (err) {
      return reject(err);
    }
  });
}

function getUserInfo() {
  return new Promise(async (resolve, reject) => {
    try {
      var oauth2 = google.oauth2({
        auth: oauth2Client,
        version: "v2",
      });

      const data = oauth2.userinfo.get((err, res) => {
        if (err) {
          return reject(err);
        }

        return resolve(res);
      });
    } catch (err) {
      return reject(err);
    }
  });
}

module.exports = {
  login: async (req, res) => {
    try {
      const user = await User.authenticate(req.body);
      const accessToken = user.generateToken();

      return res.status(200).json({
        status: true,
        message: "success logged in!",
        data: {
          login_type: "basic",
          id: user.id,
          name: user.name,
          email: user.email,
          token: accessToken,
        },
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  googleOAuth: async (req, res) => {
    try {
      const code = req.query.code;

      if (!code) {
        const loginUrl = generateAuthUrl();
        return res.redirect(loginUrl);
      }

      await setCredentials(code);

      const { data } = await getUserInfo();

      const user = {
        name: data.name,
        email: data.email,
        user_type: "google",
      };
      console.log(data);
      const emailCheck = await User.findOne({
        where: { email: data.email },
      });

      if (!emailCheck) {
        await User.create({
          f_name: data.given_name,
          l_name: data.family_name,
          email: data.email,
          role: "user",
          user_type: "google",
        });
      }

      const token = jwt.sign(user, JWT_SECRET_KEY);

      return res.status(200).json({
        status: true,
        message: "login success",
        data: {
          id: data.id,
          ...user,
          login_type: "google-oauth2",
          token,
        },
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  updateAvatar: (req, res) => {},
};
