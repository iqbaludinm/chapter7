const multer = require("multer");
const path = require("path");

const storageImage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./public/uploads/images");
  },

  filename: function (req, file, callback) {
    const namaFile = Date.now() + path.extname(file.originalname);
    callback(null, namaFile);
  },
});

const uploadImage = multer({
  storage: storageImage,
  fileFilter: (req, file, callback) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpeg" // sudah mewakili ekstensi .jpg dan .jpeg
    ) {
      callback(null, true);
    } else {
      callback(null, false);
      callback(
        new Error(
          "Selected file is not supported for upload, please choose file with .png .jpg, or .jpeg extensions"
        )
      );
    }
  },
  onError: function (err, next) {
    console.log("error", err);
    next(err);
  },
});

const storageVideo = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./public/uploads/videos");
  },

  filename: function (req, file, callback) {
    const namaFile = Date.now() + path.extname(file.originalname);
    callback(null, namaFile);
  },
});

const uploadVideo = multer({
  storage: storageVideo,
  fileFilter: (req, file, callback) => {
    if (file.mimetype == "video/mp4") {
      callback(null, true);
    } else {
      callback(null, false);
      callback(
        new Error(
          "Selected file is not supported for upload, please choose file with .mp4 extension"
        )
      );
    }
  },
  onError: function (err, next) {
    console.log("error", err);
    next(err);
  },
});

const storageFile = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./public/uploads/files");
  },

  filename: function (req, file, callback) {
    const namaFile = Date.now() + path.extname(file.originalname);
    callback(null, namaFile);
  },
});

const uploadFile = multer({
  storage: storageFile,
  fileFilter: (req, file, callback) => {
    if (
      file.mimetype == "application/pdf" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
      file.mimetype == "application/msword" ||
      file.mimetype == "application/vnd.ms-excel"
    ) {
      callback(null, true);
    } else {
      callback(null, false);
      callback(
        new Error(
          "Selected file is not supported for upload, please choose file with .pdf .doc, .xls, .docx, or .xlsx extensions"
        )
      );
    }
  },
  onError: function (err, next) {
    console.log("error", err);
    next(err);
  },
});

module.exports = {
  uploadImage,
  uploadVideo,
  uploadFile,
};
