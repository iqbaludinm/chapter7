require("dotenv").config();
const express = require("express");
const app = express();
const morgan = require("morgan");
const { PORT } = process.env;
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

app.use("/image", express.static("public/uploads/images"));
app.use("/video", express.static("public/uploads/videos"));
app.use("/file", express.static("public/uploads/files"));

app.use(express.json());
app.use(morgan("dev"));

const routes = require("./routes");
app.use("/api", routes);

app.use(function (err, req, res, next) {
  res.status(500).json({
    status: false,
    message: err.message,
    data: null,
  });
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
