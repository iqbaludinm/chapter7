const express = require("express");
const routes = express.Router();

const base = require("../controllers");
const auth = require("../controllers/authController");
const user = require("../controllers/userController");
const media = require("../controllers/mediaController");
const biodata = require("./biodata");
const history = require("./history");
const { login } = require("../middlewares");
const {
  uploadImage,
  uploadFile,
  uploadVideo,
} = require("../middlewares/upload");

routes.get("/", base.root);
routes.post("/auth/login", auth.login);

routes.post("/user/register", user.createUser);
routes.put("/user/update-avatar", login, user.updateAvatar);
routes.get("/user/profile/:id", login, user.showUser);
routes.get("/user", user.getUsers);
routes.put("/user/:id", login, user.updateUser);
routes.delete("/user/:id", login, user.deleteUser);

routes.get("/auth/google", auth.googleOAuth);

routes.post("/upload/image", login, uploadImage.single("image"), media.image);
routes.post("/upload/video", login, uploadVideo.single("video"), media.video);
routes.post("/upload/file", login, uploadFile.single("file"), media.file);

routes.use("/biodata", biodata);
routes.use("/history", history);

module.exports = routes;
